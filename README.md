# Lab9 -- Security check

## My site

<https://my.university.innopolis.ru/>

### Test Forget Password

| Test step  | Result                                                   |
|---|----------------------------------------------------------|
| Open the site | Ok                                                       |
| Push sign in button  | Auto log in, so log out and repeat                       |
| Tap "Change Password" | OK, new page opened                                      |
| Type fake email, password, new password | The account did not exist, the site complained about it  |
| Type my email, password, new password | Ok, the site updated my password and give a notification |

Both cases resulted in the same response time, but the site displayed different messages for each case. 
There was no limit on the number of attempts, which means that emails could potentially be obtained through repeated requests.

### Test Response Headers

| Test step  | Result |
|---|---|
| Open the site | Ok |
| Press F12 to see DevTools | Ok |
| Login | Ok |
| Check X-Frame-Options prseense | No header found |
| Check X-XSS-Protection presense | No header found |
| Check X-Content-Type-Options presense | No header found |
| Check Referrer-Policy | `strict-origin-when-cross-origin` |
| Check Content-Type presense | `text/html; charset=utf-8,text/html; charset=utf-8` |
| Check Set-Cookie presense | OK, It is present (too long) |

The site does not utilize any custom (X) headers, which are typically recommended for enhanced security. 
Nevertheless, it has implemented all the necessary headers in a manner that aligns with best practices.
